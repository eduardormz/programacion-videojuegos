#include "class/menu.h"
#include "class/suma.h"
#include "class/resta.h"
#include "class/multiplicacion.h"
#include "class/division.h"
#include <stdlib.h>

void main(){
	menu me;
	suma su;
	resta re;
	division di;
	mult mu;
	
	do{	
		me.solicitarOpcion();
		
		switch(me.obtenerOpcion()){
		case 1: 
			su.insertaDatos();
			su.operacion();
		    su.mostrarResultado();	    
			break;
		
		case 2:
			re.insertaDatos();
			re.operacion();
			re.mostrarResultado();
			break;
		
		case 3:
			di.insertaDatos();
			di.operacion();
			di.mostrarResultado();
			break;
			
		case 4:
			mu.insertaDatos();
			mu.operacion();
			mu.mostrarResultado();
			break;
		case 5:
			printf("\n\n Presione una tecla para salir.");
			break;
				
		default:
			printf("\n\n La opcion que eligiste no es valida.");
		
		}
	}while(me.obtenerOpcion() != 5);
	
	getch();
	
}